package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func main() {
	firewall := LoadFirewall(os.Stdin)
	severity := 0

	for pos := range firewall.Layers {
		scanPos := firewall.ScanPos(pos)
		if scanPos == 0 {
			severity += pos * firewall.Layers[pos]
		}
		firewall.Move()
	}

	fmt.Printf("trip severity is %d\n", severity)

	startT := 1
Times:
	for {
		firewall.Time = startT

		for pos := range firewall.Layers {
			scanPos := firewall.ScanPos(pos)
			if scanPos == 0 {
				startT++
				continue Times
			}
			firewall.Move()
		}

		// If we fall out here then we managed to cross without
		// being caught, so we're done.
		break
	}

	fmt.Printf("delay start time is %d\n", startT)

}

type Firewall struct {
	Time   int
	Layers []int
}

func (f *Firewall) Move() {
	f.Time++
}

func (f *Firewall) Reset() {
	f.Time = 0
}

func (f *Firewall) ScanPos(depth int) int {
	return f.ScanPosAtTime(depth, f.Time)
}

func (f *Firewall) ScanPosAtTime(depth int, time int) int {
	rng := f.Layers[depth] - 1
	if rng == 0 {
		return -1
	}
	sub := (time % (2 * rng)) - rng
	if sub < 0 {
		sub = -sub
	}
	return rng - sub
}

func LoadFirewall(r io.Reader) Firewall {
	sc := bufio.NewScanner(r)
	var firewall Firewall
	for sc.Scan() {
		line := sc.Text()
		colonIdx := strings.Index(line, ": ")
		depStr := line[:colonIdx]
		rngStr := line[colonIdx+2:]
		dep, _ := strconv.Atoi(depStr)
		rng, _ := strconv.Atoi(rngStr)
		for len(firewall.Layers) < dep {
			firewall.Layers = append(firewall.Layers, 0)
		}
		firewall.Layers = append(firewall.Layers, rng)
	}
	return firewall
}
