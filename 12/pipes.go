package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func LoadEdgeSet(r io.Reader) EdgeSet {
	ret := make(EdgeSet)
	sc := bufio.NewScanner(r)
	for sc.Scan() {
		line := sc.Text()
		delim := strings.Index(line, " <-> ")
		srcS := line[:delim]
		destsSrcS := line[delim+5:]
		src, _ := strconv.Atoi(srcS)
		destsS := strings.Split(destsSrcS, ", ")
		for _, destS := range destsS {
			dest, _ := strconv.Atoi(destS)
			ret.Add(src, dest)
		}
	}
	return ret
}

type EdgeSet map[[2]int]struct{}

func (s EdgeSet) Add(a, b int) {
	s[s.key(a, b)] = struct{}{}
}

func (s EdgeSet) Has(a, b int) bool {
	_, has := s[s.key(a, b)]
	return has
}

func (s EdgeSet) Vertices() VertexSet {
	ret := make(VertexSet)
	for edge := range s {
		ret.Add(edge[0])
		ret.Add(edge[1])
	}
	return ret
}

func (s EdgeSet) Neighbors(v int) VertexSet {
	ret := make(VertexSet)
	for edge := range s {
		if edge[0] == v {
			ret.Add(edge[1])
		}
		if edge[1] == v {
			ret.Add(edge[0])
		}
	}
	return ret
}

func (s EdgeSet) Connections(v int) VertexSet {
	stack := []int{v}
	ret := make(VertexSet)

	for len(stack) > 0 {
		var v int
		v, stack = stack[len(stack)-1], stack[:len(stack)-1]
		if !ret.Has(v) {
			ret.Add(v)
			for nv := range s.Neighbors(v) {
				stack = append(stack, nv)
			}
		}
	}

	return ret
}

func (s EdgeSet) ConnectedSubgraphCount() int {
	vertices := s.Vertices()
	ret := 0
	counted := make(VertexSet)
	for v := range vertices {
		if counted.Has(v) {
			continue
		}
		ret++
		for nv := range s.Connections(v) {
			counted.Add(nv)
		}
	}
	return ret
}

func (s EdgeSet) key(a, b int) [2]int {
	if b < a {
		a, b = b, a
	}

	return [...]int{a, b}
}

type VertexSet map[int]struct{}

func (s VertexSet) Add(v int) {
	s[v] = struct{}{}
}

func (s VertexSet) Has(v int) bool {
	_, has := s[v]
	return has
}

func main() {
	edges := LoadEdgeSet(os.Stdin)
	conns := edges.Connections(0)
	groupCount := edges.ConnectedSubgraphCount()
	fmt.Printf("There are %d nodes in the connected subgraph containing node 0\n", len(conns))
	fmt.Printf("There are %d groups\n", groupCount)
}
