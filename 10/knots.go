package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	"github.com/davecgh/go-spew/spew"
)

type KnotList []int

func (l KnotList) ReverseSlice(start, length int) {
	extent := length / 2 // integer division, so rounds down
	for i := 0; i < extent; i++ {
		idx1 := start + i
		idx2 := start + length - i - 1
		l[idx1%len(l)], l[idx2%len(l)] = l[idx2%len(l)], l[idx1%len(l)]
	}
}

func NewKnotList(length int) KnotList {
	ret := make(KnotList, length)
	for i := range ret {
		ret[i] = i
	}
	return ret
}

func part1() {
	totalLen, _ := strconv.Atoi(os.Args[1])
	l := NewKnotList(totalLen)

	lensB, _ := ioutil.ReadAll(os.Stdin)
	lensS := strings.Split(strings.TrimSpace(string(lensB)), ",")

	pos := 0
	for skip, lenS := range lensS {
		length, _ := strconv.Atoi(lenS)

		fmt.Printf("Reverse(%d, %d)\n", pos, length)
		l.ReverseSlice(pos, length)
		fmt.Printf("pos += %d + %d (+ %d = %d)\n", length, skip, length+skip, pos+length+skip)
		pos += length + skip
		skip++
	}

	spew.Dump(l)

	fmt.Printf("Check result is %d\n", l[0]*l[1])
}

func main() {
	l := NewKnotList(256)
	b, _ := ioutil.ReadAll(os.Stdin)
	b = bytes.TrimSpace(b)
	b = append(b, 17, 31, 73, 47, 23)

	pos := 0
	skip := 0
	for round := 0; round < 64; round++ {
		for _, lengthB := range b {
			length := int(lengthB)
			//fmt.Printf("Reverse(%d, %d)\n", pos, length)
			l.ReverseSlice(pos, length)
			//fmt.Printf("pos += %d + %d (+ %d = %d)\n", length, skip, length+skip, pos+length+skip)
			pos += length + skip
			skip++
		}
	}

	//spew.Dump(l)

	dense := make([]byte, 16)
	for denseI := range dense {
		sparseStart := denseI * 16
		acc := l[sparseStart]
		for sparseI := 1; sparseI < 16; sparseI++ {
			acc = acc ^ l[sparseStart+sparseI]
		}
		dense[denseI] = byte(acc)
	}

	fmt.Printf("%x\n", dense)
}
