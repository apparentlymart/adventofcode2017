package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

// Coord3D represents a position in a 3D coordinate space.
//
// The question describes movement within a hexagonal grid, but a hexagonal
// grid behaves similarly to a 3D cartesian grid if you preserve the constraint
// that X + Y + Z = 0, which our Move method does by always incrementing and
// decrementing a pair of dimensions respectively.
type Coord3D struct {
	X, Y, Z int
}

var Origin = Coord3D{0, 0, 0}

func (c Coord3D) Move(dir Direction) Coord3D {
	switch dir {
	case North:
		c.Y += 1
		c.Z -= 1
	case South:
		c.Y -= 1
		c.Z += 1
	case SouthWest:
		c.X -= 1
		c.Z += 1
	case NorthEast:
		c.X += 1
		c.Z -= 1
	case SouthEast:
		c.X += 1
		c.Y -= 1
	case NorthWest:
		c.X -= 1
		c.Y += 1
	}
	return c
}

func (c Coord3D) DistanceTo(other Coord3D) int {
	dX := c.X - other.X
	dY := c.Y - other.Y
	dZ := c.Z - other.Z
	if dX < 0 {
		dX = -dX
	}
	if dY < 0 {
		dY = -dY
	}
	if dZ < 0 {
		dZ = -dZ
	}

	// Every move in the hex grid is two moves in the cube grid, because
	// we always increment one dimension and decrement another. Therefore
	// our distance from a hex grid standpoint is half the distance from
	// a cube grid standpoint. Cube grid distances are a 3D version of
	// Manhattan Distance, which just sums up number of moves in each
	// cartesian dimension.
	return (dX + dY + dZ) / 2
}

type Direction rune

const (
	Invalid   = Direction(0)
	North     = Direction('↑')
	South     = Direction('↓')
	NorthEast = Direction('↗')
	SouthEast = Direction('↘')
	NorthWest = Direction('↖')
	SouthWest = Direction('↙')
)

func (d Direction) String() string {
	return string(d)
}

func DirectionByName(name string) Direction {
	switch name {
	case "n":
		return North
	case "s":
		return South
	case "ne":
		return NorthEast
	case "se":
		return SouthEast
	case "nw":
		return NorthWest
	case "sw":
		return SouthWest
	default:
		return Invalid
	}
}

func main() {
	inB, _ := ioutil.ReadAll(os.Stdin)
	movesS := strings.Split(string(bytes.TrimSpace(inB)), ",")

	pos := Origin
	furthest := 0
	furthestPos := Origin
	for _, moveS := range movesS {
		move := DirectionByName(moveS)
		pos = pos.Move(move)
		dist := Origin.DistanceTo(pos)
		if dist > furthest {
			furthest = dist
			furthestPos = pos
		}
	}

	shortDist := Origin.DistanceTo(pos)
	fmt.Printf("Child is at %#v, which is %d moves away\n", pos, shortDist)
	fmt.Printf("Furthest position is %#v, which is %d moves away\n", furthestPos, furthest)
}
