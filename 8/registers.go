package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"os"
	"strconv"
	"strings"

	"github.com/davecgh/go-spew/spew"
)

type Program []Instruction

type Instruction struct {
	TargetReg string
	Inc       int
	CondReg   string
	CondOp    CondOp
	CondVal   int
}

type CondOp rune

const (
	CondEqual            = CondOp('=')
	CondNotEqual         = CondOp('≠')
	CondLessThan         = CondOp('<')
	CondLessThanEqual    = CondOp('≤')
	CondGreaterThan      = CondOp('>')
	CondGreaterThanEqual = CondOp('≥')
)

func (o CondOp) String() string {
	return string(o)
}

type State map[string]int

func Execute(prog Program, state State) int {
	maxVal := int(math.MinInt32)

	for _, instr := range prog {
		spew.Dump(instr)
		condComp := state[instr.CondReg]
		var run bool

		switch instr.CondOp {
		case CondEqual:
			run = (condComp == instr.CondVal)
		case CondNotEqual:
			run = (condComp != instr.CondVal)
		case CondLessThan:
			run = (condComp < instr.CondVal)
		case CondLessThanEqual:
			run = (condComp <= instr.CondVal)
		case CondGreaterThan:
			run = (condComp > instr.CondVal)
		case CondGreaterThanEqual:
			run = (condComp >= instr.CondVal)
		}

		if run {
			state[instr.TargetReg] += instr.Inc
			if state[instr.TargetReg] > maxVal {
				maxVal = state[instr.TargetReg]
			}
		}
	}

	return maxVal
}

func Parse(r io.Reader) (Program, error) {
	var ret Program

	sc := bufio.NewScanner(r)
	for sc.Scan() {
		instr := Instruction{}
		toks := strings.Fields(sc.Text())

		instr.TargetReg = toks[0]
		instr.Inc, _ = strconv.Atoi(toks[2])
		if toks[1] == "dec" {
			instr.Inc = -instr.Inc
		}

		instr.CondReg = toks[4]
		switch toks[5] {
		case "==":
			instr.CondOp = CondEqual
		case "!=":
			instr.CondOp = CondNotEqual
		case "<":
			instr.CondOp = CondLessThan
		case "<=":
			instr.CondOp = CondLessThanEqual
		case ">":
			instr.CondOp = CondGreaterThan
		case ">=":
			instr.CondOp = CondGreaterThanEqual
		default:
			panic("what is this even")
		}
		instr.CondVal, _ = strconv.Atoi(toks[6])

		ret = append(ret, instr)
	}

	return ret, sc.Err()
}

func main() {
	prog, err := Parse(os.Stdin)
	if err != nil {
		panic(err)
	}

	state := make(State)
	maxStored := Execute(prog, state)

	spew.Dump(state)
	max := int(math.MinInt32)
	for _, v := range state {
		if v > max {
			max = v
		}
	}
	fmt.Printf("Largest register value on completion is %d\n", max)
	fmt.Printf("Largest register value during execution was %d\n", maxStored)
}
