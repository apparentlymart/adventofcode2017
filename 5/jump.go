package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func JumpOut(vects []int) int {
	pc := 0
	i := 0

	fmt.Printf("Program range is 0 - %d\n", len(vects)-1)
	for pc >= 0 && pc < len(vects) {
		delta := vects[pc]
		fmt.Printf("%03d: %d at %d -> %d\n", i, delta, pc, pc+delta)
		vects[pc]++
		pc += delta
		i++
	}

	return i
}

func JumpOutStrange(vects []int) int {
	pc := 0
	i := 0

	fmt.Printf("Program range is 0 - %d\n", len(vects)-1)
	for pc >= 0 && pc < len(vects) {
		delta := vects[pc]
		//fmt.Printf("%03d: %d at %d -> %d\n", i, delta, pc, pc+delta)
		if delta >= 3 {
			vects[pc]--
		} else {
			vects[pc]++
		}
		pc += delta
		i++
	}

	return i
}

func main() {
	sc := bufio.NewScanner(os.Stdin)
	var vects []int
	for sc.Scan() {
		delta, err := strconv.Atoi(sc.Text())
		if err != nil {
			panic(err)
		}

		vects = append(vects, delta)
	}

	i := JumpOutStrange(vects)
	fmt.Printf("Out of bounds after %d iterations\n", i)
}
