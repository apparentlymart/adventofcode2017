package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type Node struct {
	Name   string
	Weight int
}

type Tree struct {
	Nodes    map[string]*Node
	EdgesOut map[string][]string
	EdgesIn  map[string][]string
}

func (t *Tree) Add(node *Node) {
	t.Nodes[node.Name] = node
	if _, exists := t.EdgesOut[node.Name]; !exists {
		t.EdgesOut[node.Name] = nil
	}
	if _, exists := t.EdgesIn[node.Name]; !exists {
		t.EdgesIn[node.Name] = nil
	}
}

func (t *Tree) Connect(parent, child string) {
	t.EdgesOut[parent] = append(t.EdgesOut[parent], child)
	t.EdgesIn[child] = append(t.EdgesIn[child], parent)
}

func (t *Tree) Root() *Node {
	for name, edges := range t.EdgesIn {
		if len(edges) == 0 {
			return t.Nodes[name]
		}
	}
	return nil
}

func (t *Tree) AnalyzeWeights(node *Node) int {
	accumWeight := node.Weight

	weightVotes := map[int]int{}
	for _, childName := range t.EdgesOut[node.Name] {
		child := t.Nodes[childName]
		childWeight := t.AnalyzeWeights(child)
		accumWeight += childWeight
		weightVotes[childWeight]++
	}

	// If we got more than one distinct child weight then that indicates
	// an inconsistency, which we should report.
	if len(weightVotes) > 1 {
		var want, got int
		for weight, votes := range weightVotes {
			if votes > 1 {
				want = weight
			} else {
				got = weight
			}
		}
		diff := want - got

		// Look again to see which child had the wrong weight
		for _, childName := range t.EdgesOut[node.Name] {
			child := t.Nodes[childName]
			childWeight := t.AnalyzeWeights(child)
			if childWeight == got {
				needWeight := child.Weight + diff
				fmt.Fprintf(os.Stderr, "Node %q has the wrong weight %d; should be %d\n", childName, child.Weight, needWeight)
			}
		}
	}

	return accumWeight
}

func LoadTree(r io.Reader) (*Tree, error) {
	tree := &Tree{
		Nodes:    map[string]*Node{},
		EdgesOut: map[string][]string{},
		EdgesIn:  map[string][]string{},
	}
	sc := bufio.NewScanner(r)
	for sc.Scan() {
		node := &Node{}
		line := sc.Text() // lines like: ohdnhx (261) -> tgiou, lndczw
		nameEnd := strings.Index(line, " ")
		node.Name = line[:nameEnd]
		line = line[nameEnd+2:] // past the open paren
		weightEnd := strings.Index(line, ")")
		weightStr := line[:weightEnd]
		node.Weight, _ = strconv.Atoi(weightStr)
		tree.Add(node)
		fmt.Fprintf(os.Stderr, "there is a node called %q with weight %d\n", node.Name, node.Weight)

		line = line[weightEnd:] // past the closing paren

		// The string may now _optionally_ have a "->" sequence followed by a
		// list of child nodes.
		if len(line) >= 5 {
			line = line[5:] // past the " -> "
			childNames := strings.Split(line, ", ")
			for _, childName := range childNames {
				fmt.Fprintf(os.Stderr, "%q is standing on %q\n", childName, node.Name)
				tree.Connect(node.Name, childName)
			}
		}
	}

	return tree, sc.Err()
}

// Not needed for the puzzle, but fun nonetheless
func graphviz(tree *Tree) {
	fmt.Println("digraph G {")
	for name, node := range tree.Nodes {
		fmt.Printf("  %q [label=\"%s (%d)\"];\n", name, node.Name, node.Weight)
	}
	for name, parentNames := range tree.EdgesIn {
		if len(parentNames) == 0 {
			fmt.Printf("  // %q is a root\n", name)
			continue
		}

		for _, parentName := range parentNames {
			fmt.Printf("  %q -> %q;\n", parentName, name)
		}
	}
	fmt.Println("}")
}

func main() {
	tree, err := LoadTree(os.Stdin)
	if err != nil {
		panic(err)
	}

	graphviz(tree)

	fmt.Fprintf(os.Stderr, "The root of the tree is %#v\n", tree.Root())

	totalWeight := tree.AnalyzeWeights(tree.Root())
	fmt.Fprintf(os.Stderr, "The whole tree has weight %d\n", totalWeight)
}
