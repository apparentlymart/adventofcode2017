package captcha

func Calculate(in []byte) int {
	acc := 0
	for i, b := range in {
		p := in[(i+1)%len(in)]
		if b == p {
			v := int(b - '0')
			acc += v
		}
	}
	return acc
}

func Calculate2(in []byte) int {
	ofs := len(in) / 2
	acc := 0
	for i, b := range in {
		p := in[(i+ofs)%len(in)]
		if b == p {
			v := int(b - '0')
			acc += v
		}
	}
	return acc
}
