package passphrase

import (
	"bufio"
	"io"
	"sort"
	"strings"
)

func IsValid(phrase string) bool {
	words := strings.Fields(phrase)
	seen := map[string]bool{}
	for _, word := range words {
		wordB := []byte(word)
		sort.Slice(wordB, func(i, j int) bool {
			return wordB[i] < wordB[j]
		})
		word = string(wordB)
		if seen[word] {
			return false
		}
		seen[word] = true
	}
	return true
}

func ValidCount(r io.Reader) int {
	sc := bufio.NewScanner(r)
	ret := 0
	for sc.Scan() {
		if IsValid(sc.Text()) {
			ret++
		}
	}
	return ret
}
