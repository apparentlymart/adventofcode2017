package passphrase

import "testing"

func TestIsValid(t *testing.T) {
	tests := []struct {
		Input string
		Want  bool
	}{
		{"aa bb cc dd ee", true},
		{"aa bb cc dd aa", false},
		{"aa bb cc dd aaa", true},
		{"abcde fghij", true},
		{"abcde xyz ecdab", false},
		{"a ab abc abd abf abj", true},
		{"iiii oiii ooii oooi oooo", true},
		{"oiii ioii iioi iiio", false},
	}

	for _, test := range tests {
		t.Run(test.Input, func(t *testing.T) {
			got := IsValid(test.Input)
			if got != test.Want {
				t.Errorf("result is %#v; want %#v", got, test.Want)
			}
		})
	}
}
