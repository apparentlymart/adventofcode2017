
def spiral():
    spans = 1
    x = 0
    y = 0
    n = 1
    while True:
        direction = spans % 4
        length = spans / 2
        for _ in xrange(0, length):
            yield (n, x, y)
            n += 1
            if direction == 0:
                x += 1
            elif direction == 1:
                y -= 1
            elif direction == 2:
                x -= 1
            elif direction == 3:
                y += 1
        spans += 1

def part1():
    for t in spiral():
        dist = abs(t[1]) + abs(t[2])
        print t, dist
        if t[0] == 289326:
            break


def part2():
    w = 256
    d = [[0 for x in xrange(w)] for y in xrange(w)]
    center = len(d) / 2
    for _, x, y in spiral():
        rx = x + center
        ry = y + center

        if x == 0 and y == 0:
            val = 1
        else:
            val = (
                d[ry+1][rx+0] +
                d[ry+1][rx+1] +
                d[ry+0][rx+1] +
                d[ry-1][rx+1] +
                d[ry-1][rx+0] +
                d[ry-1][rx-1] +
                d[ry+0][rx-1] +
                d[ry+1][rx-1]
            )
        d[ry][rx] = val
        print (x, y, val)
        if val > 289326:
            print val
            return

part2()
