package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/davecgh/go-spew/spew"
)

type Group []Group

func parseGroup(in []byte) (Group, int, []byte) {
	var group Group
	var garbageSkipped int

	if in[0] != '{' {
		panic("group doesn't start with {")
	}
	b := in[1:] // skip initial {

	for b[0] != '}' {
		switch b[0] {
		case '{':
			var subGroup Group
			var skipped int
			subGroup, skipped, b = parseGroup(b)
			group = append(group, subGroup)
			garbageSkipped += skipped
		case '<':
			var skipped int
			skipped, b = skipGarbage(b)
			garbageSkipped += skipped
		}

		if b[0] == ',' {
			b = b[1:] // skip comma
		}
	}

	return group, garbageSkipped, b[1:] // skip final }
}

func skipGarbage(in []byte) (int, []byte) {
	if in[0] != '<' {
		panic("garbage doesn't start with <")
	}

	skipped := 0
	escape := false
	b := in[1:] // skip initial <

	for {
		var this byte
		this, b = b[0], b[1:]

		if escape {
			escape = false
			continue
		}

		switch this {
		case '>':
			return skipped, b
		case '!':
			escape = true
		default:
			skipped++
		}
	}
}

func score(group Group, parentScore int) int {
	myScore := parentScore + 1
	ret := myScore
	for _, subGroup := range group {
		ret += score(subGroup, myScore)
	}
	return ret
}

func main() {
	in, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		panic(err)
	}
	root, skipped, _ := parseGroup(in)
	spew.Dump(root)
	fmt.Printf("Score for this tree is %d\n", score(root, 0))
	fmt.Printf("Skipped %d garbage characters\n", skipped)
}
