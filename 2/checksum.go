package checksum

import (
	"bufio"
	"io"
	"math"
	"strconv"
	"strings"
)

func Checksum(r io.Reader) int {
	sc := bufio.NewScanner(r)
	acc := 0
	for sc.Scan() {
		l := sc.Text()
		nums := strings.Fields(l)
		min := math.MaxInt32
		max := 0
		for _, nS := range nums {
			n, err := strconv.Atoi(nS)
			if err != nil {
				panic(err)
			}

			if n < min {
				min = n
			}
			if n > max {
				max = n
			}
		}
		diff := max - min
		acc += diff
	}
	return acc
}

func Divisible(r io.Reader) int {
	sc := bufio.NewScanner(r)
	acc := 0
	for sc.Scan() {
		l := sc.Text()
		numsS := strings.Fields(l)
		nums := make([]int, len(numsS))
		for i, nS := range numsS {
			var err error
			nums[i], err = strconv.Atoi(nS)
			if err != nil {
				panic(err)
			}
		}

	Pairs:
		for _, a := range nums {
			for _, b := range nums {
				if a != b && (a%b) == 0 {
					acc += a / b
					break Pairs
				}
			}
		}

	}
	return acc
}
