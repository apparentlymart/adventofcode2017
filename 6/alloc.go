package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

func DetectLoop(banks []int) (int, int) {
	seen := make(BanksSet)
	log := make(BanksLog, 0, 4096)

	ret := 0

	for !seen.Has(banks) {
		seen.Add(banks)
		log.Add(banks)

		highestIdx := 0
		highestVal := banks[highestIdx]
		for i, blocks := range banks {
			if blocks > highestVal {
				highestIdx = i
				highestVal = blocks
			}
		}

		// Going to redistribute all the blocks allocated in our selected
		// bank, so it now has no blocks.
		banks[highestIdx] = 0

		for i := 0; i < highestVal; i++ {
			bankIdx := (highestIdx + i + 1) % len(banks)
			banks[bankIdx]++
		}

		ret++
	}

	lastSeen := log.Index(banks)

	return ret, ret - lastSeen
}

type BanksLog [][]int

func (l *BanksLog) Add(banks []int) {
	// Need to make a new underlying array since the caller will continue
	// to mutate the one it gave us.
	sto := make([]int, len(banks))
	copy(sto, banks)

	*l = append(*l, sto)
	fmt.Printf("at step %d we have %#v\n", len(*l)-1, sto)
}

func (l *BanksLog) Index(banks []int) int {
	for i, otherBanks := range *l {
		if banksEqual(banks, otherBanks) {
			return i
		}
	}
	return -1
}

type BanksSet map[int][][]int

func (s BanksSet) Add(banks []int) {
	k := s.hash(banks)

	// Need to make a new underlying array since the caller will continue
	// to mutate the one it gave us.
	sto := make([]int, len(banks))
	copy(sto, banks)

	s[k] = append(s[k], sto)
}

func (s BanksSet) Has(banks []int) bool {
	k := s.hash(banks)
	if bankses, exists := s[k]; exists {
		for _, otherBanks := range bankses {
			if banksEqual(banks, otherBanks) {
				return true
			}
		}
		return false
	} else {
		return false
	}
}

func (s BanksSet) hash(banks []int) int {
	ret := int(math.MaxInt64)
	for _, blocks := range banks {
		ret = ret ^ blocks
	}
	return ret
}

func banksEqual(banks []int, otherBanks []int) bool {
	for i := range banks {
		if banks[i] != otherBanks[i] {
			return false
		}
	}
	return true
}

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)
	var banks []int
	for sc.Scan() {
		blocks, err := strconv.Atoi(sc.Text())
		if err != nil {
			panic(err)
		}
		banks = append(banks, blocks)
	}
	loopIdx, loopLength := DetectLoop(banks)
	fmt.Printf("Loop detected at step %d with length %d\n", loopIdx, loopLength)
}
